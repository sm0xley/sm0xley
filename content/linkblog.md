---
title: linkblog
date: 2025-02-25
---

# linkblog

What I'm reading:

- 2025-02-25 - [A Defense of Weird Research — Asterisk](https://asteriskmag.com/issues/09/a-defense-of-weird-research)
- 2025-02-24 - [Johnny.Decimal | A system to organise your life](https://johnnydecimal.com/)
- 2025-02-23 - [When Imperfect Systems are Good, Actually: Bluesky’s Lossy Timelines](https://jazco.dev/2025/02/19/imperfection/)
- 2025-02-21 - [Multi-layered calendars](https://julian.digital/2023/07/06/multi-layered-calendars/)
- 2025-02-20 - [Creating Better Brewing Yeast With the 1011 Yeast Genomes Data Sets](https://onlinelibrary.wiley.com/doi/10.1002/yea.3990)
- 2025-02-18 - [Obsessed: A Man and His Mold](https://www.seriouseats.com/obsessed-koji-rich-shih)
- 2025-02-18 - [Kojify All the Grains](https://ourcookquest.blogspot.com/2014/09/kojify-all-grains.html)
- 2025-02-12 - [Pioreactor](https://pioreactor.com/)
- 2025-02-12 - [Controlled Mold](https://controlledmold.com/index.html)
- 2025-02-10 - [One Simple Trick™ to create inline bibliography entries with Markdown and pandoc](https://www.andrewheiss.com/blog/2023/01/09/syllabus-csl-pandoc/)
- 2025-02-10 - [DOI Citation Formatter](https://citation.doi.org/)
- 2025-02-07 - [Manual for: mhchem for MathJax, mhchem for KaTeX](https://mhchem.github.io/MathJax-mhchem/)
- 2025-02-07 - [How I'm able to take notes in mathematics lectures using LaTeX and Vim](https://castel.dev/post/lecture-notes-1/)
- 2025-02-03 - [danrlu/nextflow_cheatsheet: Tips for Nextflow and cheatsheet for channel operation](https://github.com/danrlu/Nextflow_cheatsheet)
- 2025-02-03 - [Nextflow — Nextflow documentation](https://www.nextflow.io/docs/latest/index.html)
- 2025-01-31 - [The Ghost in the MP3](https://www.theghostinthemp3.com/theghostinthemp3.html)
- 2025-01-26 - [GitFeed](http://www.gitfeed.me/)
- 2025-01-20 - [Welcome to Hell World](https://www.welcometohellworld.com/)
- 2025-01-19 - [LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com/)
- 2025-01-15 - [Near Vertical Incidence Skywave (NVIS)](https://www.hamradioschool.com/post/near-vertical-incidence-skywave-nvis)
- 2025-01-07 - [Duck, NC Water Levels - NOAA Tides & Currents](https://tidesandcurrents.noaa.gov/waterlevels.html?id=8651370)
- 2025-01-03 - [picosky](https://psky.social/)
- 2025-01-03 - [PDSls](https://pdsls.dev/at/fermentation.computer)
