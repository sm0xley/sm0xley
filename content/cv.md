---
title: CV
date: 2025-01-09
---

# CV

More formats: [cv.pdf](/cv.pdf).

## Education [](#education)

**B.S., Fermentation Science**, Appalachian State University, 2025 (anticipated) --- GPA: 3.89

-   Minors in Chemistry and Biology
-   Selected coursework: Brewing Production and Analysis, Fermentation Microbiology, Genetics, Genomics

**B.A., English**, Virginia Polytechnic Institute and State University, 2020 --- GPA: 3.69

-   Concentrations in Professional & Technical Writing and Creative Writing
-   Selected coursework: Technical Editing & Style, Science Writing, Developing Online Content

## Experience [](#experience)

**Quality Lab Intern,** Sycamore Brewing <span class="cvdate">July 2025--August 2024</span>

-   Daily checks of active fermentations including temperature, gravity, alcohol content, and pH
-   Development of improved VDK measurement procedure and data analysis workflow
-   Routine microbiological plating and qPCR on finished beers and brewing liquor
-   Yeast management: harvesting into brinks and holding tanks, preparation of fresh yeast pitches and yeast nutrient
-   Final quality checks on finished beers and manage product retain library

**Research Assistant,** App State Biology & Chemistry and Fermentation Sciences <span class="cvdate">January 2024--Present</span>

-   Literature review, experiment design, and project implementation
-   Environmental sampling, microorganism culturing and isolation
-   DNA extraction, polymerase chain reaction (PCR), and genetic differentiation of isolated yeast strains
-   Genome sequencing, yeast strain identification and characterization
-   Bioinformatics experience, including genomic data analysis and organization
-   Pilot batch production and sensory analysis of final product

**Lab Assistant,** App State Chemistry and Fermentation Sciences Service Lab <span class="cvdate">July 2023--December 2024</span>

-   Analysis of wine, must, beer, distillate, cannabinoids, and more
-   Analyses include alcohol content, pH, total and volatile acidity, SO2 levels, brix, IBU, and others
-   Aseptic procedure: microbiological plating of product and colony analysis
-   Setup and breakdown of student lab sessions and preparation of lab materials
-   Production of various fermented foods and beverages including beer, wine, cider, non-alcoholic beer, vegetable ferments, cheeses, cured meats, miso, and others


**Brewer,** Eastern Divide Brewing Company <span class="cvdate">October 2020--May 2023</span>

-   Hot side and cold side experience, from wort production through to packaging
-   Cleaning (CIP) and maintenance of all brewhouse and cellar equipment
-   Packaging experience: Keg cleaning and filling and canning line operation
-   Recipe development: developed multiple recipes for EDBC including two award-winning beers
-   Development and refinement of brewery SOPs for brewhouse operation, CIP routines, and inventory management
-   Barrel aging experience: beer aging, mixed fermentation management, and barrel CIP processes
-   Draft system cleaning and maintenance

## Skills [](#skills)

-   Analytical chemistry
-   Quality control and quality assurance
-   Microbiological control and aseptic procedure
-   DNA extraction and sequencing
-   Polymerase Chain Reaction (PCR)
-   Bioinformatics and genomic data analysis
-   Food and beverage production
-   HACCP Plan design and implementation
-   Copywriting and technical writing
-   Strong IT skillset: command line environment, bash, Python, R, and PLC programming

## Student Organizations [](#student-organizations)

**WUVT-FM 90.7: Student-run radio at Virginia Tech,** August 2016--May 2020

-   General Manager: Jun 2019--May 2020
-   Business Manager: Jun 2018--May 2019

**App State Fermentation Science Club,** August 2023--present

-   President: Aug 2024--present

## Professional Associations [](#professional-associations)

**Master Brewers Association of the Americas,** August 2023--present

**American Chemical Society,** April 2024--present

## Publications and Presentations [](#publications-and-presentations)

**"Low-alcohol Brewing Using Modified Brewhouse and Cellar Methods"**

-   Oral presentation, MBAA District Carolinas Spring Technical Meeting, May 2024
-   Oral presentation, Asheville Beer Week, May 2024

**"Brewing with Wild Appalachian Yeast Species for Low- and No-alcohol Beer Production"**

-   Poster presentation, World Brewing Congress, August 2024

## Awards [](#awards)

**Third Place Saison: Siege of Savannah Saison**, 2022 Virginia Craft Beer Cup

-   Awarded by the Virginia Craft Brewers Guild to Eastern Divide Brewing Company

**Second Place Dark European Lager: Blacksburg Black Lager**, 2023 Virginia Craft Beer Cup

-   Awarded by the Virginia Craft Brewers Guild to Eastern Divide Brewing Company
