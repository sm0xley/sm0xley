# Hello! I'm Stephen.

I'm passionate about all things fermentation.

I'm a current student and research assistant in the Fermentation Sciences program at Appalachian State University. My research focuses on bioprospecting for Appalachian yeast strains that have brewing potential.

Previously, I've worked as:

- a quality lab intern at [Sycamore Brewing](https://www.sycamorebrew.com/) in Charlotte, NC
- a lab assistant in the Fermentation Science [Service Lab](https://dcfs.appstate.edu/facilities/fermentation-facilities/service-lab) at App State
- a brewer at [Eastern Divide Brewing Company](https://easterndivide.com/) in Blacksburg, VA

In addition to my Fermentation Science degree from App State, I hold a B.A. in English with a focus in Professional and Technical Writing from Virginia Tech.

There is probably something fermenting in my kitchen.

