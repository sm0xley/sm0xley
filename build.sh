#!/usr/bin/env bash
#

# setup public/ directory, deleting any existing content
if [ -d public ]; then rm -r public; fi
cp -r content public

# update build-time
cat metadata.json \
    | jq ". += {\"build-time\": \"$(date -Iseconds)\"}" > metadata.json

# use project readme as home page and add the most recent links from linkblog.md
cp README.md public/index.md
sed -n '8,12p' content/linkblog.md >> public/index.md
echo "- [more...](/linkblog)" >> public/index.md

# choose three icons for sidebar
shuf -n 3 templates/tabler-favorites.txt \
    | xargs -I {} sh -c "cat tabler-icons/icons/{} | tr -d '\n'; echo " > templates/icons.html

pandoc_convert() {
    # takes md file as input, converts it in place to an html file and deletes the md file
    infile="$@"
    outfile="${infile%.*}.html"
    echo "converting $infile -> $outfile"
    pandoc -s \
      --title-prefix "Stephen Moxley" \
      --template templates/main.html \
      --metadata-file metadata.json \
      --output $outfile \
      $infile #&& rm $infile
}
export -f pandoc_convert

pretty_link() {
    # takes .html file as input, moves it to the pretty link path, i.e.
    # $ mv [...]/foo.html [...]/foo/index.html
    rawpath="$@"
    newdir="${rawpath%.*}"
    echo "moving $rawpath -> $newdir/index.html"
    mkdir "$newdir"
    mv "$rawpath" "$newdir/index.html"
}
export -f pretty_link

# begin .md -> .html file conversion

find public -type f -name "*.md" \
    | xargs -n 1 -P 10 -I {} bash -c 'pandoc_convert "$@"' _ {}

# use smart links for all html files except any existing "index.html" files.
# comment these lines out to retain directory structure exactly as it appears in content/

find public -type f -name "*.html" ! -name "index.html" \
    | xargs -n 1 -P 10 -I {} bash -c 'pretty_link "$@"' _ {}
