# TODO

- [x] put footer below main content
- [x] add all necessary metadata in template/main.html (og:image, twitter card, etc)
- [x] cv.md
- [x] remove color from links
- [x] fix spacing on icons in social links
- [ ] add ability for og:url/canonical URL inclusion from metadata
- [ ] new content
